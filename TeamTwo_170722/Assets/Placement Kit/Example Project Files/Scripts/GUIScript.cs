using UnityEngine;
using UnityEngine.UI;

public class GUIScript : MonoBehaviour {

    BuildManager bm;
    public GameObject penalButton;
    bool buildopen = false;

    // Use this for initialization
    void Start () {
		bm = GameObject.Find("BuildMgr").GetComponent<BuildManager>();
        //Canvas cv = GameObject.Find("Canvas").GetComponent<Canvas>();
        //cylinder = cv.transform.FindChild("CylinderButton").gameObject.GetComponent<Button>();
    }

    public void ActiveteBuilding(Button pressedBtn)
    {
        if(pressedBtn.name == "btnBuild")//if (pressedBtn.name == "BuildButton")
        {
            if (buildopen)
            {
                //bm.DeactivateBuildingmode();
                penalButton.gameObject.SetActive(false);
                //cylinder.gameObject.SetActive(false);
                pressedBtn.image.color = Color.white;
                buildopen = false;
            }
            else
            {
                //bm.ActivateBuildingmode();
                penalButton.gameObject.SetActive(true);
                //cylinder.gameObject.SetActive(true);
                pressedBtn.image.color = new Color(255, 0, 255);
                buildopen = true;

            }
        }
        else
        {
            switch (pressedBtn.name)
            {
                case "btnBuildBlock1":
                    bm.SelectBuilding(0);
                    break;
                case "btnBuildBlock2":
                    bm.SelectBuilding(1);
                    break;
                case "btnBuildBlock3":
                    bm.SelectBuilding(2);
                    break;
                case "btnBuildCar":
                    bm.SelectBuilding(3);
                    break;
    
            }

            pressedBtn.image.color = new Color(155, 120, 255);
            bm.ActivateBuildingmode();
            
        }

    }
    /*
    void Update()
    {
        if (buildopen)
        {
            if (!bm.isBuildingEnabled)
            {
                if (house.image.color != Color.white)
                    house.image.color = Color.white;

               if (car.image.color != Color.white)
                    car.image.color = Color.white;
            }
        }
    }
     * */
}
