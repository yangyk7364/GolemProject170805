﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfoManager : MonoBehaviour
{
    private static GameInfoManager s_instance = null;
    public static GameInfoManager instance
    {
        get
        {
            s_instance = FindObjectOfType(typeof(GameInfoManager)) as GameInfoManager;
            return (s_instance != null) ? s_instance : null;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        Screen.SetResolution(1920, 1080, true);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }



    //IEnumerator LoopForObj(GameObject obj)
    //{
    //    while (obj.activeSelf)
    //    {
    //        Time.timeScale = 0.0f;
    //        yield return null;
    //    }

    //}
}
