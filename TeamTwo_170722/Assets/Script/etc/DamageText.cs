﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// 적이 죽을때 얻는 점수 정보를 나타낼때 사용.
public class DamageText : MonoBehaviour
{

    public int dmage;
    public Queue<GameObject> targetQueue;
    public Sprite[] img;

    void Awake()
    {
        transform.FindChild("DamageText").GetComponent<AnimationEvent>().add = new AnimationEvent.Add(EndEvent);
    }

    void OnEnable()
    {
        if (dmage.ToString().Length > 0)
        {
            transform.FindChild("DamageText").FindChild("1").GetComponent<SpriteRenderer>().sprite = img[int.Parse(dmage.ToString("0000").Substring(3, 1))];
            transform.FindChild("DamageText").FindChild("1").gameObject.SetActive(true);
        }
        else
            transform.FindChild("DamageText").FindChild("1").gameObject.SetActive(false);

        if (dmage.ToString().Length > 1)
        {
            transform.FindChild("DamageText").FindChild("10").GetComponent<SpriteRenderer>().sprite = img[int.Parse(dmage.ToString("0000").Substring(2, 1))];
            transform.FindChild("DamageText").FindChild("10").gameObject.SetActive(true);
        }
        else
            transform.FindChild("DamageText").FindChild("10").gameObject.SetActive(false);

        if (dmage.ToString().Length > 2)
        {
            transform.FindChild("DamageText").FindChild("100").GetComponent<SpriteRenderer>().sprite = img[int.Parse(dmage.ToString("0000").Substring(1, 1))];
            transform.FindChild("DamageText").FindChild("100").gameObject.SetActive(true);
        }
        else
            transform.FindChild("DamageText").FindChild("100").gameObject.SetActive(false);

        if (dmage.ToString().Length > 3)
        {
            transform.FindChild("DamageText").FindChild("1000").GetComponent<SpriteRenderer>().sprite = img[int.Parse(dmage.ToString("0000").Substring(0, 1))];
            transform.FindChild("DamageText").FindChild("1000").gameObject.SetActive(true);
        }
        else
            transform.FindChild("DamageText").FindChild("1000").gameObject.SetActive(false);

        switch (dmage.ToString().Length)
        {
            case 1:
                transform.FindChild("DamageText").localPosition = new Vector3(-0.33f, 0.0f, 0.0f);
                break;
            case 2:
                transform.FindChild("DamageText").localPosition = new Vector3(-0.22f, 0.0f, 0.0f);
                break;
            case 3:
                transform.FindChild("DamageText").localPosition = new Vector3(-0.11f, 0.0f, 0.0f);
                break;
            case 4:
                transform.FindChild("DamageText").localPosition = new Vector3(0.0f, 0.0f, 0.0f);
                break;
        }

        transform.FindChild("DamageText").GetComponent<Animator>().enabled = true;
    }

    public void EndEvent()
    {
        transform.FindChild("DamageText").GetComponent<Animator>().enabled = false;
        gameObject.SetActive(false);
        targetQueue.Enqueue(gameObject);
    }
}
